import { useFormik } from 'formik';

import React from 'react';
import codes from 'country-calling-code';
import Details from 'src/pages/public/Login/components/Details';
import { initialValues, CompanyRegistrationSchema } from './schema';
import FormikValidationError from 'src/components/FormikValidationError';
import { OptionType, ReactSelect } from 'src/components/ReactSelect/ReactSelect';

const CompanyRegistrationForm = () => {
  const [registeredData, setRegisteredData] = React.useState(initialValues);
  const [isDataAdded, setIsDataAdded] = React.useState(false);

  const formik = useFormik({
    initialValues,
    onSubmit: (values) => {
      setRegisteredData(values);
      setIsDataAdded(true);
    },
    validationSchema: CompanyRegistrationSchema,
  });

  const onResetHandle = () => {
    setIsDataAdded(false);
    setRegisteredData(initialValues);
    formik.resetForm({ values: initialValues });
  };

  const countryOptions = codes.map((item) => {
    return {
      label: item.country,
      value: item.country,
    };
  });
  return (
    <main className="mt-5">
      <form onSubmit={formik.handleSubmit}>
        <h1 className="h3 mb-5 fw-normal text-center fw-bolder">Company Registrations</h1>
        <div className="row">
          <div className="form-floating col-md-6 mt-4">
            <input
              type="text"
              className="form-control "
              id="floatingInput"
              placeholder="name@example.com"
              name="companyName"
              onChange={formik.handleChange}
              value={formik.values.companyName}
            />
            <label className="px-4">Company Name</label>
            <FormikValidationError name="companyName" errors={formik.errors} touched={formik.touched} />
          </div>

          <div className="form-floating col-md-6 mt-4">
            <input
              type="email"
              className="form-control"
              id="floatingInput"
              placeholder="name@example.com"
              name="email"
              onChange={formik.handleChange}
              value={formik.values.email}
            />
            <label className="px-4">Email address</label>
            <FormikValidationError name="email" errors={formik.errors} touched={formik.touched} />
          </div>
        </div>

        <div className="row ">
          <div className="col-6 mt-4">
            <label>Country</label>
            <ReactSelect
              options={countryOptions}
              onChange={(value: OptionType) => formik.setFieldValue('country', value.value)}
              value={
                (countryOptions.find((item) => item.value === formik.values.country) as any) ||
                ({ value: '', label: '' } as any)
              }
            />
            <FormikValidationError name="country" errors={formik.errors} touched={formik.touched} />
          </div>

          <div className="form-floating col-md-6 mt-4">
            <input
              type="text"
              className="form-control"
              id="floatingInput"
              name="address"
              onChange={formik.handleChange}
              value={formik.values.address}
            />
            <label className="px-4">Address</label>
            <FormikValidationError name="address" errors={formik.errors} touched={formik.touched} />
          </div>
        </div>

        <div className="row ">
          <div className="form-floating col-md-6 mt-5">
            <input
              type="text"
              className="form-control"
              id="floatingInput"
              placeholder="name@example.com"
              name="phone"
              onChange={formik.handleChange}
              value={formik.values.phone}
            />
            <label className="px-4">Phone Number</label>
            <FormikValidationError name="phone" errors={formik.errors} touched={formik.touched} />
          </div>

          <div className="form-floating col-md-6 mt-5">
            <input
              type="date"
              className="form-control"
              id="floatingInput"
              placeholder="name@example.com"
              name="dateOfBirth"
              onChange={formik.handleChange}
              value={formik.values.dateOfBirth}
            />
            <label className="px-4">Date of Birth</label>
            <FormikValidationError name="dateOfBirth" errors={formik.errors} touched={formik.touched} />
          </div>
        </div>

        <div className=" mt-5">
          <label>About Yourself</label>
          <textarea
            className="form-control mt-2"
            id="floatingInput"
            placeholder="name@example.com"
            name="aboutYourself"
            onChange={formik.handleChange}
            value={formik.values.aboutYourself}
            rows={10}
          />
          <FormikValidationError name="aboutYourself" errors={formik.errors} touched={formik.touched} />
        </div>

        {isDataAdded && (
          <div className="mt-4">
            <h1 className="h3 mb-5 fw-normal text-center fw-bolder"> Recently Added Data</h1>
            <Details data={registeredData} />{' '}
          </div>
        )}

        <div className="row justify-content-end mt-5">
          <button className="col-2 btn btn-md btn-danger mx-2" type="button" onClick={onResetHandle}>
            Reset
          </button>

          <button className="col-2 btn btn-md btn-primary" type="submit">
            Submit
          </button>
        </div>
      </form>
    </main>
  );
};

export default CompanyRegistrationForm;
