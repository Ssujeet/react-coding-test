import * as Yup from 'yup';

const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const maxCharactersForAboutYourself = 300;

export const CompanyRegistrationSchema = Yup.object().shape({
  companyName: Yup.string().required('Company name is required'),
  email: Yup.string().email('Invalid email').required('Email is required'),
  address: Yup.string().required('Address is required'),
  country: Yup.string().required('Country is required'),
  phone: Yup.string().matches(phoneRegExp, 'Phone number is not valid'),
  dateOfBirth: Yup.string().required('Date of Birth is required'),
  aboutYourself: Yup.string()
    .required('Please describe about you')
    .max(maxCharactersForAboutYourself, 'Should be of max 300 characters'),
});

export const initialValues = {
  companyName: '',
  email: '',
  address: '',
  country: '',
  phone: '',
  dateOfBirth: '',
  aboutYourself: '',
};
