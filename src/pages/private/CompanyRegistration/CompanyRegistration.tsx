import React from 'react';

import { tokenName } from 'src/const';
import { useLocalStorage } from 'src/hooks';

import CompanyRegistrationForm from './Components/CompanyRegistrationForm';

interface CompanyRegistration {
  setIsLogin: (value: boolean) => void;
}
const CompanyRegistration = (props: CompanyRegistration) => {
  const [token, setToken, removeToken] = useLocalStorage(tokenName);

  const onLogout = () => {
    removeToken();
    props.setIsLogin(false);
  };

  return (
    <div>
      <button type="button" className="btn btn-outline-primary mt-4 d-flex" onClick={onLogout}>
        Logout
      </button>
      <CompanyRegistrationForm />
    </div>
  );
};

export default CompanyRegistration;
