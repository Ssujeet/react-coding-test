import React from 'react';
import LoginForm from './components/LoginForm';

interface ILogin {
  setIsLogin: (value: boolean) => void;
}
const Login = (props: ILogin) => {
  return <LoginForm setIsLogin={props.setIsLogin} />;
};

export default Login;
