import axios from 'axios';
import React from 'react';
import { useFormik } from 'formik';

import { useLocalStorage } from 'src/hooks';
import { API_URL, tokenName } from 'src/const';
import FormikValidationError from 'src/components/FormikValidationError';

import { initialValues, LoginSchema } from './schema';


interface ILoginForm {
  setIsLogin: (value: boolean) => void;
}

const LoginForm = (props: ILoginForm) => {
  const [token, setToken] = useLocalStorage(tokenName);
  const [expiresAt, setExpiresAt] = useLocalStorage('expiresAt');
  const [error, setError] = React.useState(false);

  const formik = useFormik({
    initialValues,
    onSubmit: (values) => {
      setError(false);
      const form = new FormData();
      form.append('email', values.email);
      form.append('password', values.password);
      axios
        .post(API_URL, form)
        .then((res) => {
          if (res?.data?.access_token) {
            setToken(btoa(res.data.access_token));
            setExpiresAt(btoa(res.data.expires_at));
            props.setIsLogin(true);
          }
        })
        .catch((e) => setError(true));
    },
    validationSchema: LoginSchema,
  });

  return (
    <main className="form-signin d-flex flex-column justify-content-center">
      <form onSubmit={formik.handleSubmit}>
        <h1 className="h3 mb-3 fw-normal text-center mt-5">Please Sign in</h1>
        <div className="d-flex justify-content-center">
          <div className="form-floating col-md-4">
            <input
              type="email"
              className="form-control "
              id="floatingInput"
              placeholder="name@example.com"
              name="email"
              onChange={formik.handleChange}
              value={formik.values.email}
            />
            <label>Email address</label>
            <FormikValidationError name="email" errors={formik.errors} touched={formik.touched} />
          </div>
        </div>

        <div className="d-flex justify-content-center">
          <div className="form-floating mt-2 col-md-4">
            <input
              type="password"
              className="form-control"
              id="floatingPassword"
              placeholder="Password"
              name="password"
              onChange={formik.handleChange}
              value={formik.values.password}
            />
            <label>Password</label>
            <FormikValidationError name="password" errors={formik.errors} touched={formik.touched} />
          </div>
        </div>

        <div className="row justify-content-center">
          <button className="col-6 col-md-4 btn btn-lg btn-primary mt-5" type="submit">
            Sign in
          </button>
        </div>
        {error && <p className={'text-danger text-center mx-auto mt-4'}>You don't have authorizations</p>}
      </form>
    </main>
  );
};

export default LoginForm;
