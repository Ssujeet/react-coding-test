import React from 'react';

import { getAge } from 'src/helpers/calculateAge';
import { initialValues } from 'src/pages/private/CompanyRegistration/Components/schema';

interface IDetails {
  data: typeof initialValues;
}
const Details = (props: IDetails) => {
  const { data } = props;
  return (
    <div>
      <p>Company Name: {data.companyName}</p>
      <p>Email: {data.email}</p>
      <p>Country : {data.country}</p>
      <p>Address : {data.address}</p>
      <p>Age : {isNaN(getAge(data.dateOfBirth)) ? 0 : getAge(data.dateOfBirth)}</p>

      <p>About Yourself: {data.aboutYourself}</p>
    </div>
  );
};

export default Details;
