import React from 'react';
import { tokenName } from 'src/const';

import { useLocalStorage } from 'src/hooks';
import Login from 'src/pages/public/Login/Login';
import CompanyRegistration from 'src/pages/private/CompanyRegistration/CompanyRegistration';

const Main = () => {
  const [token] = useLocalStorage(tokenName);
  const [isLogin, setIsLogin] = React.useState(false);

  return (
    <div className="container">
      {isLogin || token ? <CompanyRegistration setIsLogin={setIsLogin} /> : <Login setIsLogin={setIsLogin} />}
    </div>
  );
};

export default Main;
