import React, { ReactElement } from 'react';

interface Props {
  name: string;
  touched: { [key: string]: any | undefined };
  errors: { [key: string]: any | undefined };
  index?: number;
  keyName?: string;
  t?: any;
}

function FormikValidationError(props: Props): ReactElement {
  const { name, touched, errors } = props;

  return touched[name] && !!errors[name] ? (
    <span className={'text-danger'}> {errors[name] ? (errors[name] as string) : ''}</span>
  ) : (
    <></>
  );
}
export default FormikValidationError;
