import React, { Component } from 'react';
import Select, { CommonProps } from 'react-select';

export type OptionType = { value: string; label: string };
export type OptionsType = Array<OptionType>;

export const ReactSelect = (props: any) => <Select {...props} />;
